package com.allen.springboot.tutorial.util;

import com.allen.springboot.tutorial.entities.Author;
import com.allen.springboot.tutorial.entities.Category;
import com.allen.springboot.tutorial.entities.Quote;
import com.allen.springboot.tutorial.entities.Raw;
import com.allen.springboot.tutorial.repository.AuthorRepository;
import com.allen.springboot.tutorial.repository.CategoryRepository;
import com.allen.springboot.tutorial.repository.QuoteRepository;
import com.allen.springboot.tutorial.repository.RawRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;

@Slf4j
@Component
public class RawDao {
    @Autowired
    private RawRepository aRawRepository;
    @Autowired
    AuthorRepository authorRepo;
    @Autowired
    CategoryRepository categoryRepo;
    @Autowired
    QuoteRepository quoteRepo;


    public void saveNewQuote(){

        Random rand = new Random();

        long count = aRawRepository.count();
        int randomNum = rand.nextInt((int) count);
        Raw raw = aRawRepository.findById(randomNum).get();
        if(raw !=null) {
            log.info(raw.toString());
            Quote quote = new Quote();
            quote.setQuote(raw.getQuote());
            String author = raw.getAuthor();
            Author authorByAuthor = authorRepo.findAuthorByAuthor(author).iterator().next();
            quote.setAuthor(authorByAuthor);

            String category = raw.getCategory();
            Category categoryByName = categoryRepo.findCategoryByNameJPQL(category).get(0);
            quote.setCategory(categoryByName);
            Quote save = quoteRepo.save(quote);
            log.info("Added quote id ==>" + save.getId());
            log.info("Number of quotes==>" + quoteRepo.count());
        }

    }
}
