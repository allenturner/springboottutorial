package com.allen.springboot.tutorial.repository;

import com.allen.springboot.tutorial.entities.Author;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorRepository extends CrudRepository<Author, Integer> {


    @Query("from Author a where a.author=:authorName")
    public List<Author> findAuthorByAuthorUsingJPQL(@Param("authorName") String authorName);

    //Using Spring Data
    public List<Author> findAuthorByAuthor(String author);

    public List<Author> findByIdBetween(int start, int end);

}
