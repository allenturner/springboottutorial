package com.allen.springboot.tutorial.entities;

import lombok.Data;
import javax.validation.constraints.NotBlank;

import javax.persistence.*;


@Data
@Entity
@Table(
        name = "QUOTES"
)
public class Quote {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Integer id;

    @NotBlank(message = "Quote is mandatory")
    private String quote;
    @ManyToOne
    private Author author;
    @ManyToOne
    private Category category;
}
