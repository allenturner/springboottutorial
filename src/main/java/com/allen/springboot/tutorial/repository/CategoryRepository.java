package com.allen.springboot.tutorial.repository;

import com.allen.springboot.tutorial.entities.Category;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CategoryRepository  extends CrudRepository<Category, Integer> {
    @Query("from Category c where c.category=:categoryName")
    public List<Category> findCategoryByNameJPQL(@Param("categoryName") String categoryName);

    public List<Category> findCategoryByCategory(String category);
}
