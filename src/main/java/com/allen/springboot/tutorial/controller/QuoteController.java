package com.allen.springboot.tutorial.controller;

import com.allen.springboot.tutorial.repository.QuoteRepository;
import com.allen.springboot.tutorial.entities.Quote;
import com.allen.springboot.tutorial.util.QuoteNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RestController
public class QuoteController {

    private QuoteRepository quoteRepo;

    public QuoteController(QuoteRepository quoteRepoBean){
        this.quoteRepo = quoteRepoBean;
    }

    @GetMapping("/getQuote/{id}")
    ResponseEntity getQuoteById(@PathVariable Integer id) {
        Optional<Quote> byId = quoteRepo.findById(id);
        if(!byId.isPresent()){
            throw new QuoteNotFoundException(id);
        }else{
            return new ResponseEntity<>(byId.get(), HttpStatus.OK);
        }
    }

    @GetMapping("/quotes")
    List<Quote> all() {
        ArrayList<Quote> target = new ArrayList<>();
        Iterable<Quote> all = quoteRepo.findAll();
        all.forEach(target::add);
        return target;
    }

    @PostMapping("/addquote")
    ResponseEntity<Quote> newQuote(@Valid @RequestBody Quote quote) {
        Quote quote1 = quoteRepo.save(quote);
        return new ResponseEntity<>(quote1, HttpStatus.OK);
    }

    @PutMapping("/updatequote")
    ResponseEntity<Quote> updateQuote(@RequestBody Quote quote) {
        Quote quote1 = quoteRepo.save(quote);
        return new ResponseEntity<>(quote1, HttpStatus.OK);
    }

    @DeleteMapping("/deletequote/{id}")
    ResponseEntity<Quote> deletequote(@PathVariable Integer id) {

        quoteRepo.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, List<String>> handleValidationExceptions(MethodArgumentNotValidException ex) {

        Map<String, List<String>> errors = new HashMap<>();
        List<String> errorMsgList = new ArrayList<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errorMsgList.add(errorMessage);
            errors.put(fieldName, errorMsgList);
        });


        return errors;
    }
}
