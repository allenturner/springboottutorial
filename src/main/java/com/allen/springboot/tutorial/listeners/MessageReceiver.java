package com.allen.springboot.tutorial.listeners;

import com.allen.springboot.tutorial.repository.AuthorRepository;
import com.allen.springboot.tutorial.repository.CategoryRepository;
import com.allen.springboot.tutorial.repository.QuoteRepository;
import com.allen.springboot.tutorial.pojo.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class MessageReceiver {
    @Autowired
    private AuthorRepository authorRepo;

    @Autowired
    private CategoryRepository categoryRepo;

    @Autowired
    private QuoteRepository quoteRepo;

    @JmsListener(destination = "jms.message.endpoint")
    public void receiveMessage(Message msg)
    {
        log.info("Received " + msg );
    }
}
