package com.allen.springboot.tutorial.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "AUTHORS")
public class Author {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    private Integer id;
    private String author;

}
