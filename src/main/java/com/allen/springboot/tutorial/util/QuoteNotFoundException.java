package com.allen.springboot.tutorial.util;

public class QuoteNotFoundException extends RuntimeException{
    public QuoteNotFoundException(Integer id) {
            super("Could not find quote " + id);
    }
}
