package com.allen.springboot.tutorial.repository;

import com.allen.springboot.tutorial.entities.Raw;
import org.springframework.data.repository.CrudRepository;

public interface RawRepository extends CrudRepository<Raw, Integer> {
}
