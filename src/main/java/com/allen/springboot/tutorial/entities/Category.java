package com.allen.springboot.tutorial.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(
        name = "CATEGORIES"
)
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String category;

}

