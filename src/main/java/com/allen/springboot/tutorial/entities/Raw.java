package com.allen.springboot.tutorial.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(
        name = "RAW"
)
public class Raw {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    private Integer id;
    private String quote;
    private String author;
    private String category;

}
