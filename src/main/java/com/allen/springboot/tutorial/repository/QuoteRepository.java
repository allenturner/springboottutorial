package com.allen.springboot.tutorial.repository;

import com.allen.springboot.tutorial.entities.Quote;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface QuoteRepository extends CrudRepository<Quote, Integer> {
    public List<Quote> findQuotesByAuthor(String author);
}
