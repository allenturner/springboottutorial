package com.allen.springboot.tutorial.configuration;

import com.allen.springboot.tutorial.pojo.Teacher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan
@Configuration
public class BeanList{

    @Bean
    public Teacher teacherBean(){
        Teacher teacher = new Teacher();
        return teacher;
    }
}
