package com.allen.springboot.tutorial.pojo;

import org.springframework.beans.factory.annotation.Autowired;

public class Student {

    private String name;
    private int age;
    @Autowired
    private Teacher teacher = new Teacher();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Teacher getTeacher() {
        return teacher;
    }
    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }
}
