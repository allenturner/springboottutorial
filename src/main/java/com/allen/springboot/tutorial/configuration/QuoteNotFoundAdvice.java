package com.allen.springboot.tutorial.configuration;

import com.allen.springboot.tutorial.util.QuoteNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class QuoteNotFoundAdvice {
    @ResponseBody
    @ExceptionHandler(QuoteNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String quoteNotFoundHandler(QuoteNotFoundException ex) {
        return "Sept Msg:" + ex.getMessage();
    }

}
