package com.allen.springboot.tutorial.tasks;

import com.allen.springboot.tutorial.pojo.Message;
import com.allen.springboot.tutorial.util.RawDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component
public class ScheduledTasks {

    private final ConfigurableApplicationContext context;
    private RawDao rawDao;

    public ScheduledTasks(ConfigurableApplicationContext context, RawDao arawDao) {
        this.context = context;
        this.rawDao = arawDao;
    }

    @Scheduled(fixedRate = 5000)
    public void reportCurrentTime() {


        // Send a message
        log.info("Sending a message.");

        //Get JMS template bean reference
        JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);

        // Send a message
        System.out.println("Sending a message.");
        jmsTemplate.convertAndSend("jms.message.endpoint", new Message(1001L, "test body", new Date()));

    }

    @Scheduled(fixedRate = 5000)
    public void populateRawData() {
        rawDao.saveNewQuote();
    }


}



